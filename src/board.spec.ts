import * as Board from "./board";

describe("board.create", () => {
  it("contains an array with correct length", () => {
    const result = Board.create(2, 8, 8);
    expect(result.items).toHaveLength(10);
    expect(result.items[0]).toHaveLength(10);
  });
  it("contains an array with correct contents", () => {
    const result = Board.create(2, 4, 4);
    const expected: number[][] = [
      [-1, -1, -1, -1, -1, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, -1, -1, -1, -1, -1]
    ];
    expect(result.items[0]).toEqual([-1, -1, -1, -1, -1, -1]);
    expect(result.items).toEqual(expected);
  });
  it("contains players count", () => {
    const result = Board.create(5, 4, 4);
    expect(result).toHaveProperty("players", 5);
  });
});

describe("board.add", () => {
  it("set player's flag if requested location is empty", () => {
    const board = Board.create(2, 4, 4);
    const result = Board.add(board, 1, 3, 4);
    const expected: number[][] = [
      [-1, -1, -1, -1, -1, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 0, 0, -1],
      [-1, 0, 0, 1, 0, -1],
      [-1, -1, -1, -1, -1, -1]
    ];
    expect(result.items).toEqual(expected);
  });
  it("raise an error if requested location is not empty", () => {
    const board = Board.create(2, 4, 4);
    const modifiedBoard = Board.add(board, 1, 3, 4);
    expect(() => Board.add(modifiedBoard, 2, 3, 4)).toThrowError(
      "requested location is not empty"
    );
  });
});

describe("Board.owner", () => {
  it("returns ID 1 if 2 players have value 5", () => {
    expect(Board.owner(2, 5)).toBe(1);
  });
  it("returns ID 1 if 3 players have value 4", () => {
    expect(Board.owner(3, 4)).toBe(1);
  });
  it("returns ID 4 if 5 players have value 9", () => {
    expect(Board.owner(5, 9)).toBe(4);
  });
});
