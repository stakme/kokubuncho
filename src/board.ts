import { Board } from "./type";

/**
 * @param height
 * @param width
 * @example
 * Board.create(4, 4)
 * // {
 * //   items: [
 * //     [-1, -1, -1, -1, -1, -1],
 * //     [-1,  0,  0,  0,  0, -1],
 * //     [-1,  0,  0,  0,  0, -1],
 * //     [-1,  0,  0,  0,  0, -1],
 * //     [-1,  0,  0,  0,  0, -1],
 * //     [-1, -1, -1, -1, -1, -1],
 * //   ]
 * // }
 */
export const create = (
  players: number,
  height: number,
  width: number
): Board => {
  const boardHeight = height + 2;
  const boardWidth = width + 2;

  const items: number[][] = [];
  for (let h = 0; h < boardHeight; h++) {
    const row = Array<number>(boardWidth);
    row.fill(-1, 0, boardWidth);
    if (h === 0 || h === boardHeight - 1) {
      row.fill(-1, 0, boardWidth);
    } else {
      row[0] = -1;
      row[boardWidth - 1] = -1;
      row.fill(0, 1, boardWidth - 1);
    }
    items.push(row);
  }

  return {
    players,
    items
  };
};

export const add = (
  board: Board,
  playler: number,
  x: number,
  y: number
): Board => {
  if (board.players < playler) {
    throw new Error(
      `this board has only ${
        board.players
      } players, but player ${playler} is given`
    );
  }
  if (board.items.length < y || board.items[y].length < x) {
    throw new Error(
      `requested location (${x}, ${y}) does not exist in this board`
    );
  }
  if (board.items[y][x] !== 0) {
    throw new Error(`requested location is not empty`);
  }

  // copy array
  const items = board.items.map(i => Array.from(i));

  // take requested location
  items[y][x] =
    Math.ceil(items[y][x] / board.players) * board.players + playler;

  return { ...board, items };
};

export const owner = (players: number, value: number): number => {
  return value % players;
};

export const increment = (players: number, player: number, current: number) : number =>{
  
}
